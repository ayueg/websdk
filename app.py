from flask import Flask, render_template, send_from_directory
from flask_cors import CORS
import os


app = Flask(__name__)
CORS(app)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'app_icon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/record_playback", methods=["GET"])
def record():
    return render_template("record_playback.html")

@app.route("/playfile", methods=["GET"])
def playback():
    return render_template("playfile.html")

@app.route("/report", methods=["GET"])
def report():
    return render_template("report.html")

@app.route("/online_livestream", methods=["GET"])
def online_livestream():
    return render_template("online_livestream.html")


if __name__ == "__main__":
    app.run(port=5000, debug=True)