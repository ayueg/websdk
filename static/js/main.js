var serializeForm = function (form) {
    var obj = {};
    var formData = new FormData(form);
    for (var key of formData.keys()) {
        obj[key] = formData.get(key);
    }
    return obj;
};

function copyButton(id) {
    /* Get the text field */
    var copyText = document.getElementById(id);

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    navigator.clipboard.writeText(copyText.value);

    openToast("Status", "Copied to clipboard", 2000);
}

function openToast(head, message, timer) {

    var myToastEl = document.getElementById('liveToast');

    document.getElementById('liveToast-head').innerHTML = head;
    document.getElementById('liveToast-message').innerHTML = message;

    myToastEl.classList.add("show");
    myToastEl.classList.remove("hide");

    setTimeout(closeToast, timer, 'liveToast')
}

function closeToast() {

    var myToastEl = document.getElementById('liveToast');

    myToastEl.classList.add("hide");
    myToastEl.classList.remove("show");
}

function generateid() {
    var length = 16
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz-0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    document.getElementById("stream_token").value = result;
}

const action_record_playback = async () => {

    var form = document.getElementById('myForm');
    var myParams = JSON.stringify(serializeForm(form));

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            let responseData = JSON.parse(xmlHttp.responseText);
            openToast("Status: " + responseData["status"], responseData["message"], 2000);
        }
    }
    xmlHttp.open("POST", "http://127.0.0.1:8000/record_playback/", true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(myParams);

};

const action_playfile = async () => {

    var form = document.getElementById('myForm');
    var myParams = JSON.stringify(serializeForm(form));

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            let responseData = JSON.parse(xmlHttp.responseText);
            openToast("Status: " + responseData["status"], responseData["message"], 2000);
        }
    }
    xmlHttp.open("POST", "http://127.0.0.1:8000/playfile/", true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(myParams);

};

const action_report = async () => {

    var form = document.getElementById('myForm');
    var myParams = JSON.stringify(serializeForm(form));

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            let responseData = JSON.parse(xmlHttp.responseText);
            openToast("Status: " + responseData["status"], responseData["message"], 2000);
            if (responseData["status"] == "success") {
                document.getElementById("webreporturl").style.display = "block";
                document.getElementById("webreport").value = responseData["url"];
            }
        }
    }
    xmlHttp.open("POST", "http://127.0.0.1:8000/report/", true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(myParams);

};

const action_online_livestream = async () => {

    var form = document.getElementById('myForm');
    var myParams = JSON.stringify(serializeForm(form));

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            let responseData = JSON.parse(xmlHttp.responseText);
            console.log(responseData);
            openToast("Status: " + responseData["status"], responseData["message"], 2000);
            if (responseData["status"] == "success") {
                document.getElementById("onlinelivestreamcontainer").style.display = "block";
                document.getElementById("listenerURL").value = responseData["url_listner"];
                document.getElementById("streamerURL").value = responseData["url_streamer"];
            }
        }
    }
    xmlHttp.open("POST", "http://127.0.0.1:8000/online_stream/", true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(myParams);

};
