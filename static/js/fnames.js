function update_file_list(responseData) {
    
    if (responseData["no_files"] > 0){
        let parentelement = document.getElementById("fileselect");
        let childelement = null;
        for (let i = 0; i < responseData["no_files"]; i++) {
            childelement = document.createElement('option');
            childelement.value = responseData[i.toString()];
            childelement.innerHTML = responseData[i.toString()];
            parentelement.appendChild(childelement);
          }
    }
}

const get_fnames = async () => {

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            let responseData = JSON.parse(xmlHttp.responseText);
            if (responseData["status"] == "success") {
                update_file_list(responseData);
            }
        }
    }
    xmlHttp.open("POST", "http://127.0.0.1:8000/fnames", true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send();

};

window.onload = function() {
    get_fnames();
  };